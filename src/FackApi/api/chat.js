import User from '../../Model/User'
import Message from '../../Model/Message'
import { APPNAME } from '../../config/pluginInit'

export const Users = [
  new User({ id: 1, name: 'ChatBot', role: 'Chung', image: require('../../assets/images/user/chatbot.png'), isActive: false }),
  new User({ id: 2, name: 'Tỉnh ủy', role: 'Chung', image: require('../../assets/images/user/group.jpg'), isActive: false }),
  new User({ id: 3, name: 'Nhóm - UBND tỉnh', role: 'Chung', image: require('../../assets/images/user/group.jpg'), isActive: false }),
  new User({ id: 4, name: 'Nhóm - Sở KHĐT', role: 'Chung', image: require('../../assets/images/user/group.jpg'), isActive: true }),
  new User({ id: 5, name: 'Phùng Văn Thuận', role: 'Phó Chủ tịch UBND tỉnh', image: require('../../assets/images/user/user-01.jpg'), isActive: true }),
  new User({ id: 6, name: 'Võ Văn Chánh', role: 'Phó chủ tịch UBND P2', image: require('../../assets/images/user/VO_VAN_CHANH.jpg'), isActive: true }),
  new User({ id: 7, name: 'hoangnt', role: 'Giám đốc sở', image: require('../../assets/images/user/hoangnt.jpg'), isActive: true }),
  new User({ id: 8, name: 'Phạm Việt Phương', role: 'Chung', image: require('../../assets/images/user/user-07.jpg'), isActive: false }),
  new User({ id: 9, name: 'Nguyễn Quốc Hùng', role: 'Chung', image: require('../../assets/images/user/user-08.jpg'), isActive: true }),
  new User({ id: 10, name: 'Hoàng Văn Hà', role: 'App Chung', image: require('../../assets/images/user/user-09.jpg'), isActive: false }),
  new User({ id: 11, name: 'Nguyễn Hòa Hiệp', role: 'Chung', image: require('../../assets/images/user/user10.jpg'), isActive: true }),
  new User({ id: 12, name: 'Trần Văn Vĩnh', role: 'Chung', image: require('../../assets/images/user/user-01.jpg'), isActive: true }),
  new User({ id: 13, name: 'Hoàng Văn Hà', role: 'Chung', image: require('../../assets/images/user/hahv.png'), isActive: false }),
  new User({ id: 14, name: 'Lê Sĩ Lâm', role: 'Chung', image: require('../../assets/images/user/user-03.jpg'), isActive: false }),
  new User({ id: 15, name: 'Nguyễn Văn Hùng', role: 'Chung', image: require('../../assets/images/user/user-04.jpg'), isActive: true }),
  new User({ id: 16, name: 'Cao Tiến Dũng', role: 'Chủ tịch UBND tỉnh', image: require('../../assets/images/user/CaoTienDung.png'), isActive: true })
]

export const MessagesUser1 = [
  new Message({ text: 'How can we help? We\'re here for you! 😄', userId: 5, me: true, time: '6:45' }),
  new Message({ text: 'Hey John, I am looking for the best admin template. Could you please help me to find it out?🤔', userId: 15, me: false, time: '6:48' }),
  new Message({ text: 'Absolutely!\n' + APPNAME + ' Dashboard is the responsive bootstrap 4 admin template.', userId: 5, me: true, time: '6:50' }),
  new Message({ text: 'Looks clean and fresh UI.', userId: 15, me: false, time: '6:55' }),
  new Message({ text: 'Thanks, from ThemeForest.', userId: 5, me: true, time: '6:59' }),
  new Message({ text: 'I will purchase it for sure.', userId: 15, me: false, time: '7:05' }),
  new Message({ text: 'Okay Thanks...', userId: 5, me: true, time: '7:07' }),
  new Message({ text: 'Hey John, I am looking for the best admin template. Could you please help me to find it out?', userId: 15, me: false, time: '7:08' }),
  new Message({ text: 'Absolutely!\n' + APPNAME + ' Dashboard is the responsive bootstrap 4 admin template.', userId: 5, me: true, time: '7:10' }),
  new Message({ text: 'Looks clean and fresh UI.', userId: 15, me: false, time: '7:12' }),
  new Message({ text: 'Okay Thanks...', userId: 5, me: true, time: '7:20' })
]

export const MessagesUser2 = [
  new Message({ text: 'Test mes1', userId: 5, me: true, time: '6:45' }),
  new Message({ text: 'Test mes2', userId: 15, me: false, time: '6:48' }),
  new Message({ text: 'Test mes3', userId: 5, me: true, time: '6:50' }),
  new Message({ text: 'Test mes4', userId: 15, me: false, time: '6:55' }),
  new Message({ text: 'Test mes5', userId: 5, me: true, time: '6:59' }),
  new Message({ text: 'Test mes6', userId: 15, me: false, time: '7:05' }),
  new Message({ text: 'Test mes7', userId: 5, me: true, time: '7:07' }),
  new Message({ text: 'Test mes8', userId: 15, me: false, time: '7:08' }),
  new Message({ text: 'Test mes9', userId: 5, me: true, time: '7:10' }),
  new Message({ text: 'Test mes10', userId: 15, me: false, time: '7:12' }),
  new Message({ text: 'Test mes11', userId: 5, me: true, time: '7:20' })
]

export const MessagesUser3 = [
  new Message({ text: 'Test mes1', userId: 5, me: true, time: '6:45' }),
  new Message({ text: 'Test mes2', userId: 15, me: false, time: '6:48' }),
  new Message({ text: 'Test mes3', userId: 5, me: true, time: '6:50' }),
  new Message({ text: 'Test mes4', userId: 15, me: false, time: '6:55' }),
  new Message({ text: 'Test mes5', userId: 5, me: true, time: '6:59' }),
  new Message({ text: 'Test mes6', userId: 15, me: false, time: '7:05' }),
  new Message({ text: 'Test mes7', userId: 5, me: true, time: '7:07' }),
  new Message({ text: 'Test mes8', userId: 15, me: false, time: '7:08' }),
  new Message({ text: 'Test mes9', userId: 5, me: true, time: '7:10' }),
  new Message({ text: 'Test mes10', userId: 15, me: false, time: '7:12' }),
  new Message({ text: 'Test mes11', userId: 5, me: true, time: '7:20' })
]
export const MessagesUser4 = [
  new Message({ text: 'Test mes1', userId: 5, me: true, time: '6:45' }),
  new Message({ text: 'Test mes2', userId: 15, me: false, time: '6:48' }),
  new Message({ text: 'Test mes3', userId: 5, me: true, time: '6:50' }),
  new Message({ text: 'Test mes4', userId: 15, me: false, time: '6:55' }),
  new Message({ text: 'Test mes5', userId: 5, me: true, time: '6:59' }),
  new Message({ text: 'Test mes6', userId: 15, me: false, time: '7:05' }),
  new Message({ text: 'Test mes7', userId: 5, me: true, time: '7:07' }),
  new Message({ text: 'Test mes8', userId: 15, me: false, time: '7:08' }),
  new Message({ text: 'Test mes9', userId: 5, me: true, time: '7:10' }),
  new Message({ text: 'Test mes10', userId: 15, me: false, time: '7:12' }),
  new Message({ text: 'Test mes11', userId: 5, me: true, time: '7:20' })
]

export const MessagesUser5 = [
  new Message({ text: 'Test mes1', userId: 5, me: true, time: '6:45' }),
  new Message({ text: 'Test mes2', userId: 15, me: false, time: '6:48' }),
  new Message({ text: 'Test mes3', userId: 5, me: true, time: '6:50' }),
  new Message({ text: 'Test mes4', userId: 15, me: false, time: '6:55' }),
  new Message({ text: 'Test mes5', userId: 5, me: true, time: '6:59' }),
  new Message({ text: 'Test mes6', userId: 15, me: false, time: '7:05' }),
  new Message({ text: 'Test mes7', userId: 5, me: true, time: '7:07' }),
  new Message({ text: 'Test mes8', userId: 15, me: false, time: '7:08' }),
  new Message({ text: 'Test mes9', userId: 5, me: true, time: '7:10' }),
  new Message({ text: 'Test mes10', userId: 15, me: false, time: '7:12' }),
  new Message({ text: 'Test mes11', userId: 5, me: true, time: '7:20' })
]
