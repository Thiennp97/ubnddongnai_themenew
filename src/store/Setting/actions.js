export default {
  horizontalMenuAction (context, payload) {
    if (context.state.horizontalMenu) {
      context.commit('horizontalMenuCommit', false)
    } else {
      context.commit('horizontalMenuCommit', true)
    }
  },
  miniSidebarAction (context, payload) {
    return new Promise((resolve, reject) => {
      if (context.state.miniSidebarMenu) {
        context.commit('miniSidebarCommit', false)
        resolve(false)
      } else {
        context.commit('miniSidebarCommit', true)
        resolve(true)
      }
    })
  },
  authUserAction (context, payload) {
    context.commit('authUserCommit', payload)
  },
  addUserAction (context, payload) {
    context.commit('addUserCommit', payload)
  },
  activePageAction (context, payload) {
    context.commit('activePageCommit', payload)
  },
  addBookmarkAction (context, payload) {
    context.commit('addBookmarkCommit', payload)
  },
  removeBookmarkAction (context, payload) {
    context.commit('removeBookmarkCommit', payload)
  },
  setLangAction (context, payload) {
    context.commit('setLangCommit', payload)
  },
  layoutModeAction (context, payload) {
    const rtl = payload.rtl ? 'rtl' : 'ltr'
    document
      .querySelector('body')
      .classList.remove(...document.querySelector('body').classList)
    switch (payload.dark) {
      case 1:
        document.documentElement.style.setProperty('--iq-primary', '#991EF9')
        document.documentElement.style.setProperty(
          '--iq-primary-light',
          '#E2BEFD'
        )
        document.querySelector('body').classList.add('dark')
        break
      case 2:
        document.documentElement.style.setProperty('--iq-primary', '#475F7B')
        document.documentElement.style.setProperty('--iq-primary-light', '#FFF')
        document.querySelector('body').classList.add('light-2')
        break
      case 3:
        document.querySelector('body').classList.add('dark-2')
        document.documentElement.style.setProperty('--iq-primary', '#FFF')
        document.documentElement.style.setProperty('--iq-primary-light', '#FFF')
        break
      case 4:
        document.documentElement.style.setProperty('--iq-primary', '#475F7B')
        document.documentElement.style.setProperty('--iq-primary-light', '#FFF')
        document.querySelector('body').classList.add('light-3')
        break
      case 5:
        document.querySelector('body').classList.add('dark-3')
        break
      default:
        document.documentElement.style.setProperty('--iq-primary', '#991EF9')
        document.documentElement.style.setProperty(
          '--iq-primary-light',
          '#E2BEFD'
        )
        /* document.documentElement.style.setProperty('--iq-primary-light', '#5E04A4') */
        break
    }

    document.getElementsByTagName('html')[0].setAttribute('dir', rtl)
    context.commit('layoutModeCommit', {
      dark: payload.dark,
      rtl: payload.rtl,
      sidebar: ''
    })
  }
}
