export default {
  chatState: state => state.chat,
  videoCallState: state => state.videoCall,
  usersList: state => state.usersList,
  MessagesUser1: state => state.MessagesUser1
}
