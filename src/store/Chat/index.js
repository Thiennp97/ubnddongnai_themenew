import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  chat: [],
  usersList: [],
  MessagesUser1: [],
  videoCall: false
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
