export default {
  getDataCalender (state) {
    const dates = state.date
    switch (dates) {
      case 'week':
        return state.tuan
      case 'month':
        return state.thang
      default:
        if (state.dataCalender.filter(e => e.date === dates)[0]) {
          return state.dataCalender.filter(e => e.date === dates)[0]
        } else {
          return state.dataCalender[1]
        }
    }
  }
}
