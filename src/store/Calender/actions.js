const setCalenderActions = (context, dataCalender) => {
  context.commit('SET_DATA_CALENDER', dataCalender)
}

const setDateActions = (context, date) => {
  context.commit('SET_DATE', date)
}

export default {
  setCalenderActions,
  setDateActions
}
