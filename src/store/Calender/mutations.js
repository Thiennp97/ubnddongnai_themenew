const SET_DATA_CALENDER = (state, dataCalender) => {
  state.dataCalender = dataCalender
}

const SET_DATE = (state, date) => {
  state.date = date
}

export default {
  SET_DATA_CALENDER,
  SET_DATE
}
