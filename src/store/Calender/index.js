import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  dataCalender: [
    {
      date: '19 Tháng 4 2021',
      caNhan: 3,
      ubnd: 5,
      hdnd: 1,
      tinhUy: 0,
      soBanNganh: 12,
      diaphuong: 10,
      lamViec: 15,
      congTac: 9,
      tiepDan: 4,
      total: 28
    },
    {
      date: '20 Tháng 4 2021',
      caNhan: 2,
      ubnd: 4,
      hdnd: 3,
      tinhUy: 0,
      soBanNganh: 15,
      diaphuong: 20,
      lamViec: 19,
      congTac: 16,
      tiepDan: 7,
      total: 42
    },
    {
      date: '21 Tháng 4 2021',
      caNhan: 0,
      ubnd: 1,
      hdnd: 1,
      tinhUy: 0,
      soBanNganh: 1,
      diaphuong: 1,
      lamViec: 4,
      congTac: 0,
      tiepDan: 0,
      total: 4
    },
    {
      date: '22 Tháng 4 2021',
      caNhan: 2,
      ubnd: 7,
      hdnd: 2,
      tinhUy: 0,
      soBanNganh: 19,
      diaphuong: 22,
      lamViec: 25,
      congTac: 19,
      tiepDan: 6,
      total: 50
    },
    {
      date: '22 Tháng 4 2021',
      caNhan: 3,
      ubnd: 9,
      hdnd: 1,
      tinhUy: 0,
      soBanNganh: 20,
      diaphuong: 14,
      lamViec: 18,
      congTac: 19,
      tiepDan: 7,
      total: 44
    }
  ],
  tuan: {
    caNhan: 10,
    ubnd: 26,
    hdnd: 8,
    tinhUy: 0,
    soBanNganh: 67,
    diaphuong: 67,
    lamViec: 81,
    congTac: 63,
    tiepDan: 24
  },
  thang: {
    caNhan: 17,
    ubnd: 47,
    hdnd: 15,
    tinhUy: 0,
    soBanNganh: 122,
    diaphuong: 124,
    lamViec: 147,
    congTac: 117,
    tiepDan: 44
  },
  date: '21 Tháng 4 2021'
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
