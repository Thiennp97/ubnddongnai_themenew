import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  listData: [
    {
      noiDungPhanAnh:
        'Tôi muốn mua đất tại xã Phú Hữu, số tờ bản đồ là 30, thửa 420. Tôi muốn nhờ coi dùm khu đất này có bị quy hoạch gì hay không',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '25/03/2021',
      hanXuLy: '05/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi có thửa đất số 95, tờ bản đồ số 45 thuộc xã Thanh Bình, huyện Trảng bom diện tích 495m2 tôi có thể xin chuyển đổi thổ cư được tối đa bao nhiêu m2 và chi phí chyển đổi là bao nhiêu tiền 1m2',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '26/03/2021',
      hanXuLy: '06/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi định mua thửa đất số tờ 10, thửa 95, Phường Tam Phước, thành phố Biên Hòa có diện tích 105m2.Tôi muốn hỏi đất này có lên thổ cư được không vì giấy tờ đang là đất trồng cây lâu năm. Và nếu lên thổ cư phí sẽ mất bao nhiêu. Đất có vướng Quy hoạch không',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '26/03/2021',
      hanXuLy: '06/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Bà tôi có các thửa đất (8,9,10 thuộc tờ bản đồ số 34). Bà tôi muốn chuyển đổi mục đích lên đất ở và tách mỗi thửa ra làm 2. Tôi thấy trên bản đồ quy hoạch thể hiện là đất ở kết hợp trồng cây lâu năm. Cho tôi hỏi các thửa đất trên có đủ điều kiện chuyển mục đích và tách thửa (sau khi lên thổ cư) hay không? nếu được thì hạn mức diện tích tối thiểu chuyển mục đích của mỗi thửa là bao nhiêu',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '27/03/2021',
      hanXuLy: '07/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Hiện tại tôi đang tạm trú (có sổ tạm trú) tại Phường Trảng Dài, nhưng sổ hộ khẩu thường trú tại Phú Túc - Phú Xuyên - TP. Hà Nội. Tôi có thể làm thủ tục cấp thẻ căn cước tại Đồng Nai được không? Tôi đã tìm hiểu qua Luật Căn Cước, thì có ghi có cơ sở dữ liệu quốc gia nên người dân có thể cấp thẻ căn cước tại nơi mình cư trú hoặc tạm trú. Tôi mong được Quý cơ quan giải đáp vấn đề này. Và xin hướng dẫn cụ thể giúp tôi cách làm thẻ căn cước. Xin cám ơn',
      coQuanXuLy: 'UBND thành phố Biên Hòa',
      coQuanTiepNhan: 'Địa phương',
      ngayTiepNhan: '27/03/2021',
      hanXuLy: '07/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi dự định mua 1 thửa đất có số thửa 169, tờ bản đồ số 6, địa chỉ: xã Phú Hữu, huyện Nhơn Trạch, diện tích: 940m2, hiện tại mục đích sử dụng là đất trồng lúa. Vậy thửa đất trên có vướng quy hoạch gì không? Và nếu tôi xin chuyển mục đích sử dụng đất thành đất thổ cư thì có được không và xin chuyển được tối đa bao nhiêu m2 ạ?',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '28/03/2021',
      hanXuLy: '08/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi có 1.000m2 đất bị treo trong khu quy hoạch của AMATA khu công nghệ cao thị trấn Long Thành, huyện Long Thành, tỉnh Đồng Nai quy hoạch cách đây 10 năm chính quyền không cho mua bán, xây dựng. Cho tôi hỏi khi nào thì quy hoạch hết hiệu lực để tôi yên tâm sinh hoạt.',
      coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '28/03/2021',
      hanXuLy: '08/04/2021',
      tinhTrang: 'Đã xử lý'
    },
    {
      noiDungPhanAnh: `"Tôi có căn nhà cấp 4 ở phường Trảng Dài thuộc tờ 27, số thửa 736 xin có 2 câu hỏi như sau:
      1. Hiện tôi muốn chuyển lên thổ cư có được không? nếu được thì tôi lên thổ cư một phần hay phải lên hết.
      2. Do nhà tôi hiện đã xuống cấp, tôi muốn nâng nền và nâng mái nhà thì có phải xin giấy phép sửa chữa hay không? hồ sơ thủ tục như thế nào?"`,
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi là bác sỹ, khi đăng ký hành nghề tại tỉnh Đồng Nai theo quy định của luật khám chữa bệnh thì gửi danh sách về Sở Y tế để công bố (chúng tôi có thể tự nộp hoặc gửi bưu điện), Sở Y tế yêu cầu chúng tôi mang chứng từ gốc, đến trình diện tại Sở. Hỏi Sở Y tế có sai với chỉ thị của Thủ tướng Chính phủ là giảm thủ tục hành chính cho người dân? Đề nghị giám đốc Sở xem lại công văn này có vi hiến hay không?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Hiện tôi có mảnh đất thuộc tờ bản đồ số 10, thửa 192 ở xã Phước Khánh huyện Nhơn trạch là đất chuyên trồng lúa nước. Giờ tôi muốn chuyển mục đích sử dụng lên đất trồng cây lâu năm có được không? Và thủ tục như thế nào?',
      coQuanXuLy: 'Sở Nông nghiệp và PTNT',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi dự định mua thửa đất số thửa 19, tờ bản đồ 16, xã Phước An, huyện Nhơn Trạch. Diện tích 3997m2, mục đích sử dụng là trồng cây lâu năm. Cho tôi hỏi thửa đất này có vướng quy hoạch gì không? Việc xin tách thửa và chuyển mục đích sử dụng đất thành đất thổ cư có được không và diện tích tối đa được chuyển?',
      coQuanXuLy: 'Sở Nông nghiệp và PTNT',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Cụ thể, đèn xanh khi đi từ đường Lê Văn Duyệt thẳng qua QL51 sẽ bị trùng với đèn xanh dành cho xe máy từ trạm thu phí cầu Đồng Nai đi hướng Tam hiệp, dẫn đến 2 luồng xe cùng chạy 1 lúc, gây nguy hiểm cho cả 2 luồng nhất là xe máy. Nhiều người dân đi qua khu vực này đã, đang và sẽ gặp khó khăn, bối rối và nguy hiểm dến tính mạng khi tham gia lưu thông qua đây. Tôi mong UBND tỉnh sớm điều động, giải quyết. Tình trạng này tôi đã phản ánh cũng như gửi video cho Sở GTVT tỉnh Đồng Nai từ 3 tháng nay, mà tình trạng này ngày càng nghiêm trọng hơn. Cách đây 1 tuần có 2 vụ tai nạn giao thông thương tâm đã xẩy ra. Mong quý cơ quan giải quyết giúp dân.',
      coQuanXuLy: 'Sở Giao thông vận tải',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi là du học sinh về nước một thời gian, vậy khi đi xe máy trên địa bàn tỉnh tôi có cần thi bằng lái xe hạng A1 hay không.',
      coQuanXuLy: 'Sở Giao thông vận tải',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi dự định mua miếng đất ở xã Phước thiền, Nhơn trạch, Đồng nai. Số tờ 20, số thửa 327. Cho hỏi miếng đất này có dính quy hoạch không ? Cảm ơn',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi có mua 2 miếng đất (đã có sổ) tờ 33, thửa 68, và tờ 33, thửa 69 đất trồng cây lâu năm. Tôi muốn hỏi thửa đất này có lên thổ cư hay không?',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi đang muốn mua miếng đất thửa 122, tờ 52 xã Phước An, huyện Nhơn Trạch nhưng chưa có điều kiện xây nhà. Cho tôi hỏi khu đất đó có bắt buộc thời gian xây nhà hay không. Nếu có trong bao lâu phải xây và tôi có phải xây theo kiểu nhà phố 3 tầng, một trệt không.',
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh: `"Nguyễn Tiến Việt (Bắc Giang) được giao dự toán 150 triệu đồng để thuê tư vấn xây dựng phương án chuyển loại rừng từ rừng sản xuất sang rừng phòng hộ, đã được UBND tỉnh phê duyệt quy hoạch là rừng phòng hộ.
      Ông Việt hỏi, trường hợp này đơn vị ông có được áp dụng chỉ định thầu rút gọn theo quy định tại Điều 22 Luật Đấu thầu và Điều 54, Điều 56 Nghị định số 63/2014/NĐ-CP không?"`,
      coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh:
        'Em bên nhà thầu và có thắc mắc như sau: Trong HSMT có yêu cầu "doanh thu bình quân hằng năm", và "doanh thu bình quân hằng năm tối thiểu" thì 2 yêu cầu này có gì khác nhau không? Và cách tính là có phải lấy doanh thu của năm (thứ nhất + năm thứ 2 + năm thứ 3): 3 không? hay có gì khác nhau?',
      coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh: `"Đơn vị ông Bùi Văn Thơ đang triển khai dự án xây dựng một công trình dân dụng cấp III. Chủ đầu tư là UBND cấp xã, thực hiện bằng nguồn vốn ngân sách xã và xã hội hóa.
      Tổng mức đầu tư dự án nêu trên là 4,9 tỷ đồng. Giá trị gói thầu xây lắp là 4,2 tỷ đồng. Ông Thơ hỏi, để triển khai dự án, đơn vị ông phê duyệt kế hoạch lựa chọn nhà thầu áp dụng hình thức lựa chọn nhà thầu là chào hàng cạnh tranh có được không?"`,
      coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '15/04/2021',
      hanXuLy: '25/04/2021',
      tinhTrang: 'Chờ xử lý'
    },
    {
      noiDungPhanAnh: `"Hiện tại vợ chồng tôi có 1 lô đất 153 m2, tờ 27, thửa 963, xã phước An, huyện Nhơn trạch, tỉnh Đồng Nai. Lô đất đã được cấp sổ riêng, tách thửa là đất trồng cây lâu năm.
 
      Nhờ quý anh/Chị cho tôi hỏi lô đất của tôi có nằm trong diện quy hoạch nào không và tôi muốn chuyển mục đích sử dụng cho lô đất này thành đất ở thì có vướng mắc gì không?"`,
      coQuanXuLy: 'Sở Tài nguyên và Môi trường',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi mua đất tại xã Bình Hòa, huyện Vĩnh Cửu, tỉnh Đồng Nai. Số 17, thửa 265, là đất trồng lúa. Vậy tôi hỏi có thể chuyển qua đất cây lâu năm được không? Lệ Phí hết bao nhiêu?',
      coQuanXuLy: 'UBND huyện Vĩnh Cửu',
      coQuanTiepNhan: 'Địa phương',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh: `"Nguyễn Tiến Việt được giao dự toán 150 triệu đồng để thuê tư vấn xây dựng phương án chuyển loại rừng từ rừng sản xuất sang rừng phòng hộ, đã được UBND tỉnh phê duyệt quy hoạch là rừng phòng hộ.
      Ông Việt hỏi, trường hợp này đơn vị ông có được áp dụng chỉ định thầu rút gọn theo quy định tại Điều 22 Luật Đấu thầu và Điều 54, Điều 56 Nghị định số 63/2014/NĐ-CP không?"`,
      coQuanXuLy: 'Sở Tài chính',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '16/04/2021',
      tinhTrang: 'Quá hạn xử lý'
    },
    {
      noiDungPhanAnh:
        'Em bên nhà thầu và có thắc mắc như sau: Trong HSMT có yêu cầu "doanh thu bình quân hằng năm", và "doanh thu bình quân hằng năm tối thiểu" thì 2 yêu cầu này có gì khác nhau không? Và cách tính là có phải lấy doanh thu của năm (thứ nhất + năm thứ 2 + năm thứ 3): 3 không? hay có gì khác nhau?',
      coQuanXuLy: 'Sở Tài chính',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '16/04/2021',
      tinhTrang: 'Quá hạn xử lý'
    },
    {
      noiDungPhanAnh: `"Đơn vị ông Bùi Văn Thơ đang triển khai dự án xây dựng một công trình dân dụng cấp III. Chủ đầu tư là UBND cấp xã, thực hiện bằng nguồn vốn ngân sách xã và xã hội hóa.
      Tổng mức đầu tư dự án nêu trên là 4,9 tỷ đồng. Giá trị gói thầu xây lắp là 4,2 tỷ đồng. Ông Thơ hỏi, để triển khai dự án, đơn vị ông phê duyệt kế hoạch lựa chọn nhà thầu áp dụng hình thức lựa chọn nhà thầu là chào hàng cạnh tranh có được không?"`,
      coQuanXuLy: 'Sở Tài chính',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '16/04/2021',
      tinhTrang: 'Quá hạn xử lý'
    },
    {
      noiDungPhanAnh:
        'Hiện tôi đã hết bậc 9/9 kỹ sư hạng 3, tôi có nhu cầu học lớp bồi dưỡng chứng chỉ kỹ sư hạng 3 nhà trường khi nào mở và mở ở đâu a, Email nguyenvannghivov@gmail.com',
      coQuanXuLy: 'Sở Khoa học và Công nghệ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Kính gửi Sở KHCN. Doanh nghiệp có nhập khẩu lô hàng thép thuộc diện kiểm tra chất lượng sau thông quan theo quyết định 3810. Theo quyết định, Cơ quan kiểm tra: cơ quan, đơn vị thực hiện chức năng, nhiệm vụ về tiêu chuẩn đo lường chất lượng thuộc cơ quan chuyên môn về khoa học và công nghệ cấp tỉnh. Doanh nghiệp muốn hỏi chúng tôi có hàng hóa nhập khẩu về sử dụng tại Hà Tĩnh và được nhập khẩu qua Cửa khẩu Cảng Hải Phòng. Vậy Doanh nghiệp chúng tôi có thể lựa chọn nơi đăng ký tại Chi cục tiêu chuẩn đo lường chất lượng tỉnh hay không? Hay bắt buộc phải đăng ký ở đâu ? Và theo quy định nào ? Mong sớm nhận được trả lời của Quý Sở.',
      coQuanXuLy: 'Sở Khoa học và Công nghệ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Kính gửi Sở KH và CN, chúng tôi có nhu cầu mua robot Nhật cũ về cho công đoạn hàn, đáp ứng nhu cầu chất lượng hàng xuất khẩu và nâng cao năng lực trong sản xuất của chúng tôi. Chúng tôi tìm được máy robot Hitachi M6100 sản xuất năm 1989, còn đang hoạt động tốt. Vậy cho chúng tôi hỏi có thể nhập khẩu được robot này về không để chúng tôi đặt mua và đem về phục vụ cho sản xuất hàng gang thép của chúng tôi. Trân trọng cảm ơn!',
      coQuanXuLy: 'Sở Khoa học và Công nghệ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Kính gửi Sở Khoa học và Công nghệ. Bên em là doanh nghiệp có vốn 100% Hàn Quốc, là doanh nghiệp thường chịu thuế VAT 10% . do có nhu cầu mở rộng sản xuất (sản xuất theo ngành nghề chứng nhận đầu tư) nên đang có ý định nhập khẩu thêm máy móc là máy tiện kim loại Mã HS 8458 và máy mài kim loại mã HS 8460 trên 10 năm(khoảng 16-17 năm) từ HÀN QUỐC.Vậy cho e xin được hỏi là công ty e có được nhập khẩu những máy móc đó về Việt Nam theo phụ lục I của quyết định 18 không ạ? Trước khi nhập khẩu có cần làm thủ tục hay chuẩn bị chứng từ gì gửi Sở Khoa Học Công nghệ không a?Em mong quý Sở trả lời để bên em có thể nhanh chóng nhập khẩu lô hàng này ạ. Em xin chân thành cảm ơn!',
      coQuanXuLy: 'Sở Khoa học và Công nghệ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Theo các quy định tại Nghị định số 16/2015/NĐ-CP ngày 14/02/2015 và số 141/NĐ-CP ngày 10/10/2016, tại đơn vị sự nghiệp tự đảm bảo chi thường xuyên quy định chức danh lãnh đạo đơn vị sự nghiệp. Xin cho tôi hỏi vậy các chức danh này gồm người đứng đầu (Giám đốc), cấp Phó người đứng đầu (Phó Giám đốc) và bao gồm cả Trưởng phòng, Phó trưởng phòng chuyên môn cấu thành đơn vị sự nghiệp không ạ? Xin cảm ơn và mong nhận được phản hồi, giải đáp của Sở Nội vụ!',
      coQuanXuLy: 'Sở Nội vụ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Thủ tướng Chính phủ có ban hành Quyết định số 1900/QĐ-TTg về việc công nhận xã đảo thuộc tỉnh Sóc Trăng, trong đó có các xã, thị trấn thuộc huyện Cù Lao Dung, tỉnh Sóc Trăng. Như vậy, nơi tôi công tác có thuộc phạm vi áp dụng tại Điểm b Khoản 2 Điều 1 của Nghị định 76/2019/NĐ-CP ngày 08/10/2019 của Chính phủ về chính sách đối với cán bộ, công chức, viên chức, người lao động và người hưởng lương trong lực lượng vũ trang công tác ở vùng có điều kiện kinh tế - xã hội đặc biệt khó khăn hay không? Kính đề nghị Sở Nội vụ hướng dẫn',
      coQuanXuLy: 'Sở Nội vụ',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi là giáo viên, Đảng viên. Năm nay, tôi sinh con thứ ba, tổ chức Đảng xử lý tôi với hình thức khiển trách, Nhà trường xét chậm tăng lương 3 tháng. Vậy, tôi bị như trên là đúng hay sai? có phù hợp theo Nghị định 176/2013/NĐ-CP không?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Hiện, Bệnh viện nơi vợ tôi từng điều trị có đầy đủ hồ sơ giấy tờ chứng minh việc sinh con thứ 3 là ngoài ý muốn của vợ chồng tôi, nếu cơ quan có yêu cầu xác minh. Vậy, trường hợp sinh con thứ 3 của tôi có bị kỷ luật không?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi đang công tác tại khoa y của một trường cao đẳng có đào tạo ngành điều dưỡng. Văn bằng của tôi là cử nhân điều dưỡng. Vậy tôi có thể làm chứng chỉ hành nghề điều dưỡng không? Và thủ tục như thế nào?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi tốt nghiệp cao đẳng chuyên ngành xét nghiệm do Bộ Y tế cấp bằng, đã đi làm hơn 2 năm tại phòng khám chuyên khoa và phòng khám đa khoa. Hiện đang làm tại phòng xét nghiệm của 1 phòng khám đa khoa. Nay, tôi muốn được cấp chứng chỉ hành nghề có được không?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    },
    {
      noiDungPhanAnh:
        'Tôi đang công tác tại khoa y của một trường cao đẳng có đào tạo ngành điều dưỡng. Văn bằng của tôi là cử nhân điều dưỡng. Vậy tôi có thể làm chứng chỉ hành nghề điều dưỡng không? Và thủ tục như thế nào?',
      coQuanXuLy: 'Sở Y tế',
      coQuanTiepNhan: 'Sở ban ngành',
      ngayTiepNhan: '10/04/2021',
      hanXuLy: '20/04/2021',
      tinhTrang: 'Đang xử lý'
    }
    // {
    //   noiDungPhanAnh: '',
    //   coQuanXuLy: '',
    //   coQuanTiepNhan: 'Sở ban ngành',
    //   ngayTiepNhan: '',
    //   hanXuLy: '',
    //   tinhTrang: ''
    // }
  ],
  dataShow: [],
  ojMenuLeft: [
    {
      label: 'Tổng quan',
      code: 'Tổng quan',
      number: undefined,
      colorNumber: undefined,
      child: []
    },
    {
      label: 'Cơ quan xử lý',
      code: 'Cơ quan xử lý',
      number: 35,
      colorNumber: 'badge default-b-badge badge-secondary',
      head: true
    },
    {
      label: 'VP UBND tỉnh',
      code: 'VP UBND tỉnh',
      number: 0,
      colorNumber: 'badge badge-secondary',
      child: []
    },
    {
      label: 'Sở ban ngành',
      code: 'Sở ban ngành',
      number: 33,
      colorNumber: 'badge badge-success',
      active: false,
      child: [
        {
          label: 'Sở Tài chính',
          code: 'Sở Tài chính',
          number: 3,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Kế hoạch và Đầu tư',
          code: 'Sở Kế hoạch và Đầu tư',
          number: 4,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Nông nghiệp và Phát triển Nông thôn',
          code: 'Sở Nông nghiệp và Phát triển Nông thôn',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Công thương',
          code: 'Sở Công thương',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Xây dựng',
          code: 'Sở Xây dựng',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Giao thông vận tải',
          code: 'Sở Giao thông vận tải',
          number: 2,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Tài nguyên và Môi trường',
          code: 'Sở Tài nguyên và Môi trường',
          number: 10,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Khoa học và Công nghệ',
          code: 'Sở Khoa học và Công nghệ',
          number: 4,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Giáo dục và Đào tạo',
          code: 'Sở Giáo dục và Đào tạo',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Y tế',
          code: 'Sở Y tế',
          number: 6,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Văn hóa, Thể thao và Du lịch',
          code: 'Sở Văn hóa, Thể thao và Du lịch',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Lao động - Thương binh và Xã hội',
          code: 'Sở Lao động - Thương binh và Xã hội',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Tư pháp',
          code: 'Sở Tư pháp',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Thông tin và Truyền thông',
          code: 'Sở Thông tin và Truyền thông',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Nội vụ',
          code: 'Sở Nội vụ',
          number: 2,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Sở Ngoại vụ',
          code: 'Sở Ngoại vụ',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Thanh tra tỉnh',
          code: 'Thanh tra tỉnh',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Ban quản lý khu công nghệ cao',
          code: 'Ban quản lý khu công nghệ cao',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Ban quản lý các khu công nghiệp Đồng Nai',
          code: 'Ban quản lý các khu công nghiệp Đồng Nai',
          number: 0,
          colorNumber: 'badge badge-success'
        },
        {
          label: 'Ban Dân tộc',
          code: 'Ban Dân tộc',
          number: 0,
          colorNumber: 'badge badge-success'
        }
      ]
    },
    {
      label: 'Địa phương',
      code: 'Địa phương',
      number: 2,
      colorNumber: 'badge badge-info',
      active: false,
      child: [
        {
          label: 'UBND thành phố Biên Hòa',
          code: 'UBND thành phố Biên Hòa',
          number: 1,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND thành phố Long Khánh',
          code: 'UBND thành phố Long Khánh',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Tân Phú',
          code: 'UBND huyện Tân Phú',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Vĩnh Cửu',
          code: 'UBND huyện Vĩnh Cửu',
          number: 1,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Định Quán',
          code: 'UBND huyện Định Quán',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Trảng Bom',
          code: 'UBND huyện Trảng Bom',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Thống Nhất',
          code: 'UBND huyện Thống Nhất',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Cẩm Mỹ',
          code: 'UBND huyện Cẩm Mỹ',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Long Thành',
          code: 'UBND huyện Long Thành',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Nhơn Trạch',
          code: 'UBND huyện Nhơn Trạch',
          number: 0,
          colorNumber: 'badge badge-info'
        },
        {
          label: 'UBND huyện Xuân Lộc',
          code: 'UBND huyện Xuân Lộc',
          number: 0,
          colorNumber: 'badge badge-info'
        }
      ]
    },
    {
      label: 'Tình trạng',
      code: 'Tình trạng',
      number: 35,
      colorNumber: 'badge default-b-badge badge-secondary',
      head: true
    },
    {
      label: 'Chờ xử lý',
      code: 'Chờ xử lý',
      number: 12,
      colorNumber: 'badge badge-primary',
      child: []
    },
    {
      label: 'Đang xử lý',
      code: 'Đang xử lý',
      number: 13,
      colorNumber: 'badge badge-secondary',
      child: []
    },
    {
      label: 'Đã xử lý',
      code: 'Đã xử lý',
      number: 7,
      colorNumber: 'badge badge-success',
      child: []
    },
    {
      label: 'Quá hạn xử lý',
      code: 'Quá hạn xử lý',
      number: 3,
      colorNumber: 'badge badge-danger',
      child: []
    }
    // {
    //   label: 'Đến hạn xử lý',
    //   code: 'Đến hạn xử lý',
    //   number: 0,
    //   colorNumber: 'badge badge-secondary',
    //   child: []
    // },
    // {
    //   label: 'Trong hạn xử lý',
    //   code: 'Trong hạn xử lý',
    //   number: 0,
    //   colorNumber: 'badge badge-dark',
    //   child: []
    // }
  ],
  activeMenu: undefined,
  ojThongKe: [
    {
      label: 'Sở Tài chính',
      number: 0
    },
    {
      label: 'Sở Kế hoạch và Đầu tư',
      number: 0
    },
    {
      label: 'Sở Nông nghiệp và Phát triển Nông thôn',
      number: 0
    },
    {
      label: 'Sở Công thương',
      number: 0
    },
    {
      label: 'Sở Xây dựng',
      number: 0
    },
    {
      label: 'Sở Giao thông vận tải',
      number: 0
    },
    {
      label: 'Sở Tài nguyên và Môi trường',
      number: 0
    },
    {
      label: 'Sở Khoa học và Công nghệ',
      number: 0
    },
    {
      label: 'Sở Giáo dục và Đào tạo',
      number: 0
    },
    {
      label: 'Sở Y tế',
      number: 0
    },
    {
      label: 'Sở Văn hóa, Thể thao và Du lịch',
      number: 0
    },
    {
      label: 'Sở Lao động - Thương binh và Xã hội',
      number: 0
    },
    {
      label: 'Sở Tư pháp',
      number: 0
    },
    {
      label: 'Sở Thông tin và Truyền thông',
      number: 0
    },
    {
      label: 'Sở Nội vụ',
      number: 0
    },
    {
      label: 'Sở Ngoại vụ',
      number: 0
    },
    {
      label: 'Thanh tra tỉnh',
      number: 0
    },
    {
      label: 'Ban quản lý khu công nghệ cao',
      number: 0
    },
    {
      label: 'Ban quản lý các khu công nghiệp Đồng Nai',
      number: 0
    },
    {
      label: 'Ban Dân tộc',
      number: 0
    }
    // {
    //   label: 'Sở Tài nguyên và Môi trường',
    //   number: 0
    // },
    // {
    //   label: 'UBND thành phố Biên Hòa',
    //   number: 0
    // },
    // {
    //   label: 'Sở Kế hoạch và Đầu tư',
    //   number: 0
    // },
    // {
    //   label: 'Sở Y tế',
    //   number: 0
    // },
    // {
    //   label: 'Sở Nông nghiệp và PTNT',
    //   number: 0
    // },
    // {
    //   label: 'Sở Giao thông vận tải',
    //   number: 0
    // },
    // {
    //   label: 'UBND huyện Vĩnh Cửu',
    //   number: 0
    // },
    // {
    //   label: 'Sở Tài chính',
    //   number: 0
    // },
    // {
    //   label: 'Sở Khoa học và Công nghệ',
    //   number: 0
    // },
    // {
    //   label: 'Sở Nội vụ',
    //   number: 0
    // }
  ]
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
