export default {
  // addProjectCommit (state, data) {
  //   state.projectList.push(data)
  // },
  // selectedCommit (state, data) {
  //   switch (data.type) {
  //     case 'project':
  //       state.selectedProject = data.data
  //       break
  //     case 'category':
  //       state.selectedCategory = data.data
  //       break
  //   }
  // },
  // addTaskCommit (state, data) {
  //   state.taskList.push(data)
  // },
  // updateStatusCommit (state, data) {
  //   const taskIndex = state.taskList.findIndex(task => task.id === data.id)
  //   if (data.task_status) {
  //     data.task_status = false
  //   } else {
  //     data.task_status = true
  //   }
  //   state.taskList[taskIndex] = data
  // }
  loadData (state) {
    state.dataShow.push(state.listData)
    state.activeMenu = state.ojMenuLeft[0]
    loadThongKe(state)
  },
  setShowData (state, data) {
    state.dataShow = data
  },
  addShowData (state, data) {
    state.dataShow.push(data)
  },
  setMenuLeft (state, data) {
    state.activeMenu = data
  },
  setActiveMenu (state, o) {
    state.ojMenuLeft[o.i].active = o.b
  },
  setNML (state, o) {
    state.ojMenuLeft[o.i].number = o.number
  },
  setNML2 (state, o) {
    state.ojMenuLeft[o.i].child[o.i2].number = o.number
  }
}

export function loadThongKe (state) {
  for (let i = 0; i < state.ojThongKe.length; i++) {
    const o = state.ojThongKe[i]
    const o2 = state.listData.filter(f => f.coQuanXuLy === o.label)
    o.number = o2.length
  }
}
