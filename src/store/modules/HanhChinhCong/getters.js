export default {
  listHoSoHanhChinh: state => state.listHoSoHanhChinh,
  listSoBanNganh: state => state.listSoBanNganh,
  listDiaPhuong: state => state.listDiaPhuong
}
