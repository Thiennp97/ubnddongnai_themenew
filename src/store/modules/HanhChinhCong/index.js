import dataFake from './dataFake'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
const state = {
  listSoBanNganh: dataFake.listSoBanNganh,
  listHoSoHanhChinh: dataFake.listHoSoHanhChinh,
  listDiaPhuong: dataFake.listDiaPhuong
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
