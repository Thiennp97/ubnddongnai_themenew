const listHoSoHanhChinh = [
  {
    maHoSo: '000.00.02.G05-210419-0082',
    thuTuc: 'Thủ tục cấp giấy phép thành lập và công nhận Điều lệ quỹ',
    chuHoSo: 'Hộ kinh doanh Dương Bá Ngọc',
    coQuanXuLy: 'Sở Công thương',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0083',
    thuTuc:
      'Cấp giấy phép hoạt động bán lẻ điện đến cấp điện áp 0,4kV tại địa phương',
    chuHoSo: 'Hộ kinh doanh Hoàng Hà Nga',
    coQuanXuLy: 'Sở Công thương',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0084',
    thuTuc: 'Cấp lại chứng chỉ năng lực hoạt động xây dựng hạng II, III',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư',
    coQuanXuLy: 'Sở Xây dựng',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0085',
    thuTuc: 'Thông báo sửa đổi, bổ sung nội dung chương trình khuyến mại',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Tâm Sen',
    coQuanXuLy: 'Sở Công thương',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0086',
    thuTuc: 'Thông báo thay đổi thông tin của cổ đông sáng lập công ty cổ phần',
    chuHoSo: 'Cửa hàng Đức Huy',
    coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0087',
    thuTuc:
      'Chuyển đổi công ty cổ phần thành công ty trách nhiệm hữu hạn một thành viên',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Tâm An',
    coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
    ngayTiepNhan: '17/04/2021',
    hanXuLy: '19/04/2021',
    trangThai: 'Quá hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0088',
    thuTuc:
      'Cấp giấy phép rời cảng, bến thủy nội địa đối với phương tiện, thủy phi cơ',
    chuHoSo: 'Công ty TNHH Thịnh Hoàng',
    coQuanXuLy: 'Cảng vụ Đường thủy nội địa khu vực I',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0089',
    thuTuc:
      'Chấp thuận thiết kế kỹ thuật và phương án tổ chức thi công của nút giao đấu nối vào quốc lộ',
    chuHoSo: 'Công ty tnhh dịch vụ và phát triển công nghệ Hà Đinh',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0090',
    thuTuc:
      'Quyết định thu hồi tài sản công trong trường hợp thu hồi tài sản công theo quy định tại các điểm a, b, c, d, đ và e khoản 1 Điều 41 của Luật Quản lý, sử dụng tài sản công.',
    chuHoSo: 'Công ty tnhh dịch vụ và phát triển công nghệ Nam Hà',
    coQuanXuLy: 'Bộ, cơ quan ngang Bộ, cơ quan thuộc Chính phủ',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0091',
    thuTuc:
      'Thông báo lập địa điểm kinh doanh (đối với doanh nghiệp tư nhân, công ty TNHH, công ty cổ phần, công ty hợp danh)',
    chuHoSo: 'Hộ kinh doanh Nga Mai',
    coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0092',
    thuTuc: 'Phê duyệt phương án tổ chức giao thông trên đường cao tốc',
    chuHoSo: 'Công ty TNHH Thịnh Nam',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0093',
    thuTuc: 'Bổ nhiệm công chứng viên',
    chuHoSo: 'Hà Tiến Minh',
    coQuanXuLy: 'Bộ Tư pháp',
    ngayTiepNhan: '18/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đã xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0094',
    thuTuc: 'Cấp Giấy phép vận tải qua biên giới Campuchia - Lào - Việt Nam',
    chuHoSo: 'Công ty TNHH Xây Dựng Hòa Nam',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0095',
    thuTuc:
      'Cấp Giấy phép loại A, E hoặc giấy phép loại B, C, F, G lần đầu trong năm',
    chuHoSo: 'Công ty TNHH Xây Dựng Nam Minh',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0096',
    thuTuc: 'Cấp giấy nhận chứng bị nhiễm HIV do tai nạn rủi ro nghề nghiệp',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Thế Kỉ Mới',
    coQuanXuLy: 'Trung tâm Kiểm soát bệnh tật tỉnh',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0097',
    thuTuc:
      'Cấp giấy phép vận tải đường bộ quố tế GMS cho phương tiện của các doanh nghiệp, hợp tác xã',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Thành Đô',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0098',
    thuTuc:
      'Thông báo lập địa điểm kinh doanh (đối với doanh nghiệp tư nhân, công ty TNHH, công ty cổ phần, công ty hợp danh)',
    chuHoSo: 'Hộ kinh doanh Nga Mai',
    coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0099',
    thuTuc: 'Cấp lại Giấy chứng nhận đăng ký phương tiện',
    chuHoSo: 'Vũ Thị Oanh',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0100',
    thuTuc:
      'Cấp lại Giấy phép vận tải qua biên giới Campuchia - Lào - Việt Nam',
    chuHoSo: 'Công ty TNHH Xây Dựng Nam Minh',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0101',
    thuTuc: 'Cấp lại Giấy phép xe tập lái',
    chuHoSo: 'Nguyễn Nam Minh',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '20/04/2021',
    trangThai: 'Đến hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0102',
    thuTuc:
      'Cấp lại giấy chứng nhận trung tâm sát hạch lái xe đủ điều kiện hoạt động',
    chuHoSo: 'Công ty TNHH Minh Hòa',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0103',
    thuTuc:
      'Sáp nhập doanh nghiệp (đối với công ty TNHH, công ty cổ phần và công ty hợp danh)',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Hòa Bình',
    coQuanXuLy: 'Sở Kế hoạch và Đầu tư',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0104',
    thuTuc:
      'Cấp lại giấy phép đào tạo lái xe ô tô trong trường hợp điều chỉnh hạng xe đào tạo, lưu lượng đào tạo',
    chuHoSo: 'Công ty TNHH Minh Nam',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Đang xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0105',
    thuTuc: 'Cấp phép thi công nút giao đấu nối vào quốc lộ',
    chuHoSo: 'Công ty TNHH Xây Dựng Nam Hà',
    coQuanXuLy: 'Sở Giao thông vận tải',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0106',
    thuTuc:
      'Cấp lại, đổi, điều chỉnh thông tin trên sổ bảo hiểm xã hội, thẻ bảo hiểm y tế',
    chuHoSo: 'Công ty TNHH Bắc Hà',
    coQuanXuLy: 'Sở Y tế',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0107',
    thuTuc:
      'Kê khai, thẩm định tờ khai phí bảo vệ môi trường đối với nước thải',
    chuHoSo: 'Công ty TNHH Môi trường Xanh',
    coQuanXuLy: 'Sở Tài nguyên và Môi trường',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0108',
    thuTuc:
      'Cấp Chứng chỉ hành nghề luật sư đối với người được miễn đào tạo nghề luật sư, miễn tập sự hành nghề luật sư',
    chuHoSo: 'Văn phòng Luật  Huy Hoàng',
    coQuanXuLy: 'Sở Tư pháp',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0109',
    thuTuc:
      'Cấp Chứng chỉ hành nghề luật sư đối với người đạt yêu cầu kiểm tra kết quả tập sự hành nghề luật sư',
    chuHoSo: 'Văn phòng Luật  Tiến Thịnh',
    coQuanXuLy: 'Sở Tư pháp',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  },
  {
    maHoSo: '000.00.02.G05-210419-0110',
    thuTuc:
      'Bổ sung, thay đổi thông tin của thuốc đã kê khai, kê khai lại trong trường hợp có thay đổi so với thông tin đã được công bố nhưng giá thuốc không đổi',
    chuHoSo: 'Trung tâm hỗ trợ doanh nghiệp, tư vấn xúc tiến đầu tư Thành Đô',
    coQuanXuLy: 'Sở Y tế',
    ngayTiepNhan: '19/04/2021',
    hanXuLy: '21/04/2021',
    trangThai: 'Trong hạn',
    tinhTrang: 'Chờ xử lý'
  }
]

const listSoBanNganh = [
  {
    ma: 'STC',
    ten: 'Sở Tài chính'
  },
  {
    ma: 'SKH-DT',
    ten: 'Sở Kế hoạch và Đầu tư'
  },
  {
    ma: 'SNN-PTNT',
    ten: 'Sở Nông nghiệp và Phát triển Nông thôn'
  },
  {
    ma: 'SCT',
    ten: 'Sở Công thương'
  },
  {
    ma: 'SXD',
    ten: 'Sở Xây dựng'
  },
  {
    ma: 'SGTVT',
    ten: 'Sở Giao thông vận tải'
  },
  {
    ma: 'STN-MT',
    ten: 'Sở Tài nguyên và Môi trường'
  },
  {
    ma: 'SKH-CN',
    ten: 'Sở Khoa học và Công nghệ'
  },
  {
    ma: 'SGD-DT',
    ten: 'Sở Giáo dục và Đào tạo'
  },
  {
    ma: 'SYT',
    ten: 'Sở Y tế'
  },
  {
    ma: 'SVHTT-DL',
    ten: 'Sở Văn hóa - Thể thao và Du lịch'
  },
  {
    ma: 'SLDTB-XH',
    ten: 'Sở Lao động - Thương binh và Xã hội'
  },
  {
    ma: 'STP',
    ten: 'Sở Tư pháp'
  },
  {
    ma: 'STT-TT',
    ten: 'Sở Thông tin và Truyền thông'
  },
  {
    ma: 'SNoiVu',
    ten: 'Sở Nội vụ'
  },
  {
    ma: 'SNgoaiVu',
    ten: 'Sở Ngoại vụ'
  },
  {
    ma: 'TTT',
    ten: 'Thanh tra tỉnh'
  },
  {
    ma: 'BQLKCNC',
    ten: 'Ban quản lý khu công nghệ cao'
  },
  {
    ma: 'BQLKCNDN',
    ten: 'Ban quản lý các khu công nghiệp Đồng Nai'
  },
  {
    ma: 'BDT',
    ten: 'Ban Dân tộc'
  }
]

const listDiaPhuong = [
  {
    ma: 'BienHoa',
    ten: 'UBND thành phố Biên Hòa'
  },
  {
    ma: 'LongKhanh',
    ten: 'UBND thành phố Long Khánh'
  },
  {
    ma: 'TanPhu',
    ten: 'UBND Huyện Tân Phú'
  },
  {
    ma: 'DinhQuan',
    ten: 'UBND huyện Định Quán'
  },
  {
    ma: 'TrangBom',
    ten: 'UBND huyện Trảng Bom'
  },
  {
    ma: 'ThongNhat',
    ten: 'UBND huyện Thống Nhất'
  },
  {
    ma: 'CamMy',
    ten: 'UBND huyện Cẩm Mỹ'
  },
  {
    ma: 'LongThanh',
    ten: 'UBND huyện Long Thành'
  },
  {
    ma: 'NhonTrach',
    ten: 'UBND huyện Nhơn Trạch'
  },
  {
    ma: 'VinhCuu',
    ten: 'UBND huyện Vĩnh Cửu'
  }
]
export default { listHoSoHanhChinh, listSoBanNganh, listDiaPhuong }
