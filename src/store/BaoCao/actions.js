export default {
  filterData (context, payload) {
    /* return new Promise((resolve, reject) => {
      context.commit('addProjectCommit', payload)
      resolve()
    }) */
    return context.commit('filterData', payload)
  },
  showDashboard (context, payload) {
    return context.commit('showDashboard', payload)
  }
}
