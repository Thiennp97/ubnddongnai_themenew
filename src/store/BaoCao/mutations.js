import data from './fakeData/data'
export default {
  filterData (state, filter) {
    let res = data.filter(t => filter.type === -1 || t.Type === filter.type)
    res = res.filter(t => filter.unitId === -1 || t.UnitId === filter.unitId)
    state.baoCaoData = res
  },
  showDashboard (state, data) {
    state.showDashboard = data
  }
}
