import Vue from 'vue'
import Vuex from 'vuex'
import Setting from './Setting/index'
import Ecommerce from './Ecommerce/index'
import qlnv from './QuanLyNhiemVu/index'

import HanhChinhCong from './modules/HanhChinhCong'
import YKienNguoiDan from './modules/YKienNguoiDan'
import Calender from './Calender/index'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    Setting,
    Ecommerce,
    qlnv,
    HanhChinhCong,
    YKienNguoiDan,
    Calender
  },
  state: {},
  mutations: {},
  actions: {},
  getters: {},
  strict: debug
})
