const setIsShowDetail = (context, isShowDetail) => {
  context.commit('SET_IS_SHOW_DETAIL', isShowDetail)
}

export default {
  setIsShowDetail
}
