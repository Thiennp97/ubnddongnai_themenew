const SET_IS_SHOW_DETAIL = (state, isShowDetail) => {
  state.isShowDetail = isShowDetail
}
export default {
  SET_IS_SHOW_DETAIL
}
